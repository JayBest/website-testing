<html>
<?php session_start();  ?>


<head>
  <title>Website Testing Platform</title>
  <meta charset="UTF-8">
  <meta name="viewport? content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>


<style>

body {
  margin: 0px;
}

header {
  background-color: #515c66;
  padding: 35px;
}

h2 {
  font-size: 25pt;
  padding: 35px;
  margin-top: 0px;
  text-align: center;
  color: #284959 ;
  font-family: Helvetica;
}

h3 {
  font-size: 20pt;

  margin-top: 0px;
  text-align: center;
  color: #284959 ;
  font-family: Helvetica;
}

p {
  color: #284959;
  font-family: Helvetica;
  font-size: 15pt;
  text-align: center;
}

.main {
  margin-left: 25%;
  margin-right: 25%;
  text-align: left;
}

a {
  text-decoration: none;
}

.form {
  font-size: 18px;
  text-align: left;
  height: 30px;
  color: #284959 ;
}

.dropdown {
  float: right;
  text-align: left;
  font-weight: bold;
  font-size: 15px;
  padding-left: 10px;
  width: 100%;
  height: 40px;
  color: black;
  background-color: #ebeded;
  border: 1px solid #ebeded;
  transition-duration: 0.4s;
}

.dropdown:hover {
  opacity: 0.7;
}

.textbox {
  float: right;
  text-align: center;
  font-weight: bold;
  font-size: 15px;
  padding-left: 10px;
  width: 20%;
  height: 40px;
  color: black;
  background-color: #ebeded;
  border: 1px solid #ebeded;
  transition-duration: 0.4s;
}

.textbox:hover {
  opacity: 0.7;
}

.buttonNext {
  float: right;
  border: none;
  background-color: #8ac656;
  color: white;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 10px;
  width: 100px;
  height: 40px;
  transition-duration: 0.4s;
}

.buttonNext:hover {
  opacity: 0.7;
}

.buttonBack {
  float: left;
  border: none;
  background-color: #8ac656;
  color: white;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 10px;
  width: 100px;
  height: 40px;
  transition-duration: 0.4s;
}

.buttonBack:hover {
  opacity: 0.7;
}

.buttonBlack1 {
  float: left;
  border: none;
  background-color: #515c66;
  color: white;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 10px;
  width: 150px;
  height: 60px;
  transition-duration: 0.4s;
}

.buttonBlack2 {
  margin-left: 40%;
  border: none;
  background-color: #515c66;
  color: white;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 10px;
  width: 150px;
  height: 60px;
  transition-duration: 0.4s;
}

.buttonBlack3 {
  float: right;
  border: none;
  background-color: #515c66;
  color: white;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 10px;
  width: 150px;
  height: 60px;
  transition-duration: 0.4s;
}

.arrow {
  margin-left: 22.5%;
  height: 100;
  width: 100;
}

heading-previous {
  float: left;
  font-size: 20pt;
  width: 20%;
  text-align: center;
  color: #284959 ;
  font-family: Helvetica;
}

heading-new {
  float: right;
  font-size: 20pt;
  width: 20%;
  text-align: center;
  color: #284959 ;
  font-family: Helvetica;
}

.result-previous {
  float: left;
  text-align: center;
  font-weight: bold;
  font-size: 15px;
  padding-left: 10px;
  width: 20%;
  height: 40px;
  color: black;
  background-color: #ebeded;
  border: 1px solid #ebeded;
  transition-duration: 0.4s;
}

.result-previous:hover {
  opacity: 0.7;
}

.result-new {
  float: right;
  text-align: center;
  font-weight: bold;
  font-size: 15px;
  padding-left: 10px;
  width: 20%;
  height: 40px;
  color: black;
  background-color: #ebeded;
  border: 1px solid #ebeded;
  transition-duration: 0.4s;
}

.result-new: hover {
  opacity: 0.7;
}

</style>

<body>

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "testcalc";
$useridp1 = $_SESSION["user"];
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM `calculation` WHERE `UserID` = $useridp1 ORDER BY CalculationDate DESC LIMIT 2";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	
	$nums = array("", "");
	$i = 0;
while($row = $result->fetch_assoc()) {
	$nums[$i] = $row["AllVechicleCost"];
		$i++;
    }
    
} else {
    echo "0 results";
}

$conn->close();
session_destroy();
?>

  <header></header>
  <h2><b>
    Total Road Incident Cost Calculator
  </b></h2>



 <div class="main">


        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

        <heading-previous><b>Previous Calculation</b></heading-previous>
        <img class="arrow" src="arrow-right.png">
        <heading-new><b>New Calculation</b></heading-new>




        <br>
        <br>

        <input class="result-previous" name="result-previous" value=<?php echo $nums[1] ?>></input>
        <input class="result-new" name="result-new" value=<?php echo $nums[0] ?>></input>


        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

        <p>
            Here shows the previous calculation results and the new calculation results.
            The number displays the total incident costs for all vehicles.
           
        </p>

        <br>
        <br>
        <br>
        <br>


      <input type="button" class="buttonBlack2" value="Restart" onClick="window.location.href = 'index.html'" />
 
  </div>


</html>
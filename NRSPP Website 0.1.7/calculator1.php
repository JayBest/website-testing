<?php session_start();


?>
<html>
   <head>
      <title>Website Testing Platform</title>
      <meta charset="UTF-8">
      <meta name="viewport? content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   </head>
   <style>
      body {
      margin: 0px;
      }
      header {
      background-color: #515c66;
      padding: 35px;
      }
      h2 {
      font-size: 25pt;
      padding: 35px;
      margin-top: 0px;
      text-align: center;
      color: #284959 ;
      font-family: Helvetica;
      }
      h3 {
      font-size: 25pt;
      padding: 35px;
      margin-top: 0px;
      text-align: center;
      color: #284959 ;
      font-family: Helvetica;
      }
      p {
      color: #284959;
      font-family: Helvetica;
      font-size: 10pt;
      text-align: center;
      }
      .main {
      margin-left: 20%;
      margin-right: 20%;
      text-align: center;
      }
      a {
      text-decoration: none;
      }
      .form {
      font-size: 18px;
      text-align: left;
      height: 30px;
      color: #284959 ;
      }
      .dropdown {
      float: right;
      text-align: left;
      font-weight: bold;
      font-size: 15px;
      padding-left: 10px;
      width: 100%;
      height: 40px;
      color: black;
      background-color: #ebeded;
      border: 1px solid #ebeded;
      transition-duration: 0.4s;
      }
      .dropdown:hover {
      opacity: 0.7;
      }
      .textbox {
      float: right;
      text-align: left;
      font-weight: bold;
      font-size: 15px;
      padding-left: 10px;
      width: 100%;
      height: 40px;
      color: black;
      background-color: #ebeded;
      border: 1px solid #ebeded;
      transition-duration: 0.4s;
      }
      .textbox:hover {
      opacity: 0.7;
      }
      .buttonNext {
      float: right;
      border: none;
      background-color: #8ac656;
      color: white;
      padding-top: 5px;
      padding-bottom: 5px;
      padding-right: 10px;
      width: 100px;
      height: 40px;
      transition-duration: 0.4s;
      }
      .buttonNext:hover {
      opacity: 0.7;
      }
      .buttonBack {
      float: left;
      border: none;
      background-color: #8ac656;
      color: white;
      padding-top: 5px;
      padding-bottom: 5px;
      padding-right: 10px;
      width: 100px;
      height: 40px;
      transition-duration: 0.4s;
      }
      .buttonBack:hover {
      opacity: 0.7;
      }
   </style>
   <body>
      <header></header>
      <h2><b>
         Total Road Incident Cost Calculator
         </b>
      </h2>
      <div class="main-background">
      <div class="main">
         <br>
         <br>
         <form id="form1" class="form" method="POST" action="calculator2.php">
            <label for="sector">What Is Your Industry Sector?</label>
            <select id="choice" name="dropdownSector" class="dropdown">
               <option value="gm">General Manufacturing</option>
               <option value="alf">Agriculture, Livestock and Foresty</option>
               <option value="sfb">Services - Food and Beverages</option>
               <option value="sugt">Services - Utilities e.g. Gas and Telecommunications</option>
               <option value="sbf">Services - Banking and Finance</option>
               <option value="cat">Contruction and Associated Trades</option>
               <option value="pt">Public Transport</option>
               <option value="tih">Transport - Interstate and Heavy Haulage</option>
               <option value="tid">Transport - Interstate Haulage and Distribution</option>
               <option value="tlf">Transport - Light Haulage and Frieght</option>
               <option value="tps">Transport - Postal Services</option>
               <option value="tmf">Transport - Mixed Frieght</option>
               <option value="ot">Other Transport</option>
               <option value="om">Other Manufacturing</option>
               <option value="o">Other</option>
			   
               </label>
            </select>
         
         <br>
         <br>
         <br>
         <br>
         
            <label for="mainRevenue">What Is Your Main Revenue Earner?</label>
            <input id="mainR" type="text" name="mainRevenue" class="textbox"></input>
         </form>
         <br>
         <br>
         <br>
         <br>
         <input type="button" class="buttonBack" value="BACK" onClick="window.location.href = 'calcIntro.html'" />
         <input id="Next" type="button" class="buttonNext" value="NEXT" onClick="submitform()" />
      </div>


      <script>
function submitform()
{
document.getElementById("form1").submit();

}
</script>
</html>
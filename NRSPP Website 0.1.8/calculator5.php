<?php session_start();

$_SESSION['FleetDistance'] = $_POST['FleetDistance'];
$_SESSION['AnnualInsurancePremium'] = $_POST['AnnualInsurancePremium'];
$_SESSION['ExcessPerClaim'] = $_POST['ExcessPerClaim'];
print_r( $_SESSION ); 



?>
<html>


<head>
  <title>Website Testing Platform</title>
  <meta charset="UTF-8">
  <meta name="viewport? content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>


<style>

body {
  margin: 0px;
}

header {
  background-color: #515c66;
  padding: 35px;
}

h2 {
  font-size: 25pt;
  padding: 35px;
  margin-top: 0px;
  text-align: center;
  color: #284959 ;
  font-family: Helvetica;
}

h3 {
  font-size: 25pt;
  padding: 35px;
  margin-top: 0px;
  text-align: center;
  color: #284959 ;
  font-family: Helvetica;
}

p {
  color: #284959;
  font-family: Helvetica;
  font-size: 10pt;
  text-align: center;
}

.main {
  margin-left: 20%;
  margin-right: 20%;
  text-align: center;
}

a {
  text-decoration: none;
}

.form {
  font-size: 18px;
  text-align: left;
  height: 30px;
  color: #284959 ;
}

.dropdown {
  float: right;
  text-align: left;
  font-weight: bold;
  font-size: 15px;
  padding-left: 10px;
  width: 100%;
  height: 40px;
  color: black;
  background-color: #ebeded;
  border: 1px solid #ebeded;
  transition-duration: 0.4s;
}

.dropdown:hover {
  opacity: 0.7;
}

.textbox {
  float: right;
  text-align: left;
  font-weight: bold;
  font-size: 15px;
  padding-left: 10px;
  width: 100%;
  height: 40px;
  color: black;
  background-color: #ebeded;
  border: 1px solid #ebeded;
  transition-duration: 0.4s;
}

.textbox:hover {
  opacity: 0.7;
}

.buttonNext {
  float: right;
  border: none;
  background-color: #8ac656;
  color: white;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 10px;
  width: 100px;
  height: 40px;
  transition-duration: 0.4s;
}

.buttonNext:hover {
  opacity: 0.7;
}

.buttonBack {
  float: left;
  border: none;
  background-color: #8ac656;
  color: white;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 10px;
  width: 100px;
  height: 40px;
  transition-duration: 0.4s;
}

.buttonBack:hover {
  opacity: 0.7;
}

</style>

<body>
  <header></header>
  <h2><b>
    Total Road Incident Cost Calculator
  </b></h2>


  <div class="main-background">
  <div class="main">
        
      <br>
      <br>

    <form id="form5" class="form" method="POST" action="calculatorResult.php">


  <label for="GreyFleet">Total Number of Vehicles In Grey Fleet:</label>
  <input type="number" name="GreyFleet" class="textbox" placeholder="0"></input>

      <br>
      <br>
      <br>
      <br>

  <label for="GreyFleetDistance">Annual Total Distance Travelled By Grey Fleet: (kms)</label>
  <input type="number" name="GreyFleetDistance" class="textbox" placeholder="0.00"></input>

      <br>
      <br>
      <br>
      <br>

    <input type="button" class="buttonBack" value="BACK" onClick="window.location.href = 'calculator4.php'" />
    <input type="button" class="buttonNext" value="NEXT" onClick="submitform()" />

    </div>
<script>
function submitform()
{
document.getElementById("form5").submit();

}
</script>



</html>